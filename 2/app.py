

input = open("input.txt", "r")
lines = input.readlines()

current = 5
code = ""

for i in lines :
    for j in i :
        if j == "U":
            if  current not in [1,2,3]:
                current = current - 3
        elif j=="D":
            if  current not in [7,8,9]:
                current = current + 3
        elif j == "R" :
            if current not in [3,6,9]:
                current = current + 1
        elif j == "L" :
            if current not in [1,4,7]:
                current = current - 1
    code = code + str(current)

print code