

input = open("input.txt", "r")
lines = input.readlines()

current = 5
code = ""

for i in lines :
    for j in i :
        if j == "U":
            if  current in [6,7,8,10,11,12]:
                current = current - 4
            elif current == 3 or current == 13:
                current -= 2
        elif j=="D":
            if  current in [2,3,4,6,7,8]:
                current = current + 4
            elif current == 11 or current == 1:
                current+=2
        elif j == "R" :
            if current not in [1,4,9,12,13]:
                current = current + 1
        elif j == "L" :
            if current not in [1,2,5,10,13]:
                current = current - 1
    #print current
    code = code + str(hex(current).split('0x')[1]).upper()

print code